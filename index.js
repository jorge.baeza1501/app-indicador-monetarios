const readline = require('readline');

var lector = readline.createInterface({
	input: process.stdin,
	output: process.stdout
});

function menu(){
    console.log("\nMENU");
    console.log("1. Actualizar datos");
    console.log("2. Promedio últimos 5 archivos");
    console.log("3. Mínimo y máximo últimos 5 archivos");
    console.log("4. Salir");

    lector.question('\nEscriba una opción: ', opcion =>{
		switch(opcion){
			case '1':
				break;
			case '2':
				break;
			case '3':
				break;
			case '4':
				lector.close();
				process.exit(0);
				break;
			default:
				console.log('Opción no encontrada');
				menu();
				break;
		}
    });
    
}

menu();